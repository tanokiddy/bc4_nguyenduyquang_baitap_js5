//BT1
document.getElementById("btnDiemTongKet").onclick = function () {
  var diemMon1 = document.getElementById("diemMon1").value * 1;
  var diemMon2 = document.getElementById("diemMon2").value * 1;
  var diemMon3 = document.getElementById("diemMon3").value * 1;
  var diemKhuVuc = document.getElementById("diemKhuVuc").value * 1;
  var diemDoiTuong = document.getElementById("diemDoiTuong").value * 1;
  var diemChuan = document.getElementById("diemChuan").value * 1;
  var diemTongKet = diemMon1 + diemMon2 + diemMon3 + diemDoiTuong + diemKhuVuc;
  console.log("diemTongKet: ", diemTongKet);
  var ketQua = "";

  if (
    diemTongKet >= diemChuan &&
    diemMon1 !== 0 &&
    diemMon2 !== 0 &&
    diemMon3 !== 0
  ) {
    ketQua = "Trúng tuyển";
  } else {
    ketQua = "Không trúng tuyển";
  }
  document.getElementById("ketQuaTuyenSinh").innerHTML =
    "Điểm Tổng kết: " + diemTongKet + "<br>Kết quả: " + ketQua;
};

//BT2
document.getElementById("btnTinhTienDien").onclick = function () {
  var soKw = document.getElementById("soKw").value * 1;
  var tienDien = 0;
  if (soKw < 0) {
    tienDien = "Vui lòng nhập chính xác tiền điện";
  } else if (soKw >= 0 && soKw <= 50) {
    tienDien = 500 * soKw;
  } else if (soKw > 50 && soKw <= 100) {
    tienDien = 500 * 50 + 650 * (soKw - 50);
  } else if (soKw > 100 && soKw <= 200) {
    tienDien = 500 * 50 + 650 * 50 + 850 * (soKw - 100);
  } else if (soKw > 200 && soKw <= 350) {
    tienDien = 500 * 50 + 650 * 50 + 850 * 100 + 1100 * (soKw - 200);
  } else {
    tienDien =
      500 * 50 + 650 * 50 + 850 * 100 + 1100 * 150 + 1300 * (soKw - 350);
  }
  document.getElementById("ketQuaTienDien").innerHTML =
    "Tiền điện: " + tienDien.toLocaleString();
};
